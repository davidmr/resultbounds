package com.garbagecode.resultbounds;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.junit.Test;

import com.garbagecode.resultbounds.dialect.MySqlDialect;

public class AbstractPaginationInterceptorTest {

  @Test
  public void testSetProperties() {
    AbstractPaginationInterceptor interceptor = new PaginationInterceptor();
    Properties properties = new Properties();
    properties.setProperty(AbstractPaginationInterceptor.PROPERTY_DIALECT_NAME, "com.garbagecode.resultbounds.dialect.MySqlDialect");
  
    interceptor.setProperties(properties);
    assertTrue(AbstractPaginationInterceptor.getDialect() instanceof MySqlDialect);
  }
  
}
