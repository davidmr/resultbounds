package com.garbagecode.resultbounds;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.garbagecode.resultbounds.bounds.ResultBoundsTest;
import com.garbagecode.resultbounds.bounds.TotalCountBoundsTest;
import com.garbagecode.resultbounds.dialect.MySqlDialectTest;

@RunWith(Suite.class)
@SuiteClasses({ PaginationInterceptorTest.class, AbstractPaginationInterceptorTest.class,
  ResultBoundsTest.class, TotalCountBoundsTest.class, MySqlDialectTest.class })
public class AllTests {

}
