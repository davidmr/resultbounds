package com.garbagecode.resultbounds;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import com.garbagecode.resultbounds.bounds.ResultBounds;
import com.garbagecode.resultbounds.test.MyBatisTestCase;
import com.garbagecode.resultbounds.test.User;
import com.garbagecode.resultbounds.test.mapper.UserMapper;

public class PaginationInterceptorTest extends MyBatisTestCase {

  @Test
  public void test() {
    SqlSession session = openSession();
    
    {
      ResultBounds resultBounds = new ResultBounds(3, 5);
      resultBounds.setOrder("age.desc");
      
      List<User> userList = session.selectList("test.user.selectUser", null, resultBounds);
      
      assertEquals(5, userList.size());
      assertEquals("刘一", userList.get(0).getName());
      assertEquals("张三", userList.get(1).getName());
      assertEquals("王五", userList.get(2).getName());
      assertEquals("孙七", userList.get(3).getName());
      assertEquals("赵六", userList.get(4).getName());
    }
    
    // 对 Mapper 接口分页
    {
      ResultBounds resultBounds = new ResultBounds(3, 5);
      resultBounds.setOrder("age.desc");
      
      List<User> userList = resultBounds.apply(()->{
        return session.getMapper(UserMapper.class).getUserList();
      });
      
      assertEquals(5, userList.size());
      assertEquals("刘一", userList.get(0).getName());
      assertEquals("张三", userList.get(1).getName());
      assertEquals("王五", userList.get(2).getName());
      assertEquals("孙七", userList.get(3).getName());
      assertEquals("赵六", userList.get(4).getName());
    }
  }
  
  @Test
  public void testGetResultBounds() {
    PaginationInterceptor interceptor = new PaginationInterceptor();
    
    assertNull(interceptor.getResultBounds(null));
    
    RowBounds rowBounds = new RowBounds();
    ResultBounds resultBounds = new ResultBounds();
    
    assertNotSame(rowBounds, interceptor.getResultBounds(rowBounds));
    assertSame(resultBounds, interceptor.getResultBounds(resultBounds));
  }
  
  @Test
  public void testIsNeedPagination() {
    PaginationInterceptor interceptor = new PaginationInterceptor();
    ResultBounds resultBounds = null;
    
    {
      resultBounds = new ResultBounds();
      assertFalse(interceptor.isNeedPagination(resultBounds));
    }
    
    // 从第 10 页开始
    {
      resultBounds = new ResultBounds(10, RowBounds.NO_ROW_LIMIT);
      assertTrue(interceptor.isNeedPagination(resultBounds));
    }
    
    // 只获取 10 条记录
    {
      resultBounds = new ResultBounds(RowBounds.NO_ROW_OFFSET, 10);
      assertTrue(interceptor.isNeedPagination(resultBounds));
    }
    
    // 对 age 降序排序
    {
      resultBounds = new ResultBounds();
      resultBounds.setOrder("age.desc");
      assertTrue(interceptor.isNeedPagination(resultBounds));
    }
    
    {
      resultBounds = new ResultBounds();
      resultBounds.setOrder("");
      assertFalse(interceptor.isNeedPagination(resultBounds));
    }
    
    {
      resultBounds = new ResultBounds();
      resultBounds.setOrder("  ");
      assertFalse(interceptor.isNeedPagination(resultBounds));
    }
  }
  
}
