package com.garbagecode.resultbounds.bounds;

import org.apache.ibatis.session.SqlSession;

import static org.junit.Assert.*;

import org.junit.Test;

import com.garbagecode.resultbounds.test.MyBatisTestCase;

public class TotalCountBoundsTest extends MyBatisTestCase {

  @Test
  public void testGetTotal() {
    SqlSession session = openSession();
    TotalCountBounds bounds = new TotalCountBounds("test.user.selectUser", null);
    
    assertEquals(10, bounds.getTotal(session));
  }
  
}
