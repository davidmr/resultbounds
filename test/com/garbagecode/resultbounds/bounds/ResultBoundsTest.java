package com.garbagecode.resultbounds.bounds;

import org.apache.ibatis.session.SqlSession;

import static org.junit.Assert.*;

import org.junit.Test;

import com.garbagecode.resultbounds.test.MyBatisTestCase;
import com.garbagecode.resultbounds.test.mapper.UserMapper;

public class ResultBoundsTest extends MyBatisTestCase {

  @Test
  public void testGetTotal() {
    SqlSession session = openSession();
    
    {
      ResultBounds resultBounds = new ResultBounds(5, 5);
      session.selectList("test.user.selectUser", null, resultBounds);
      assertEquals(10L, resultBounds.getTotal(session));
    }
    
    {
      ResultBounds resultBounds = new ResultBounds(5, 5);
      resultBounds.apply(()->{
        return session.getMapper(UserMapper.class).getUserList();
      });
      assertEquals(10L, resultBounds.getTotal(session));
    }
  }
  
  @Test
  public void testApply() {
    ResultBounds resultBounds = new ResultBounds();
    
    assertNull(ResultBounds.getCurrentBounds());
    
    resultBounds.apply(() -> {
      assertNotNull(ResultBounds.getCurrentBounds());
      return null;
    });
    
    assertNull(ResultBounds.getCurrentBounds());
  }
  
}
