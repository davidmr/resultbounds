package com.garbagecode.resultbounds.spring;

import org.apache.ibatis.session.SqlSession;

import com.garbagecode.resultbounds.bounds.TotalCountBounds;

public class TotalCountBoundsBean extends TotalCountBounds {
  private static SqlSession sqlSession;

  public TotalCountBoundsBean() {
    super(null, null);
  }
  
  public TotalCountBoundsBean(String statement, Object parameter) {
    super(statement, parameter);
  }
  
  public void setSqlSession(SqlSession sqlSession) {
    TotalCountBoundsBean.sqlSession = sqlSession;
  }

  @Override
  public long getTotal(SqlSession sqlSession) {
    return sqlSession == null 
        ? super.getTotal(TotalCountBoundsBean.sqlSession) 
        : super.getTotal(sqlSession);
  }

}
