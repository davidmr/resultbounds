package com.garbagecode.resultbounds.spring;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import com.garbagecode.resultbounds.PaginationInterceptor;
import com.garbagecode.resultbounds.bounds.ResultBounds;

@Intercepts({ @Signature(type = Executor.class, 
method = "query", 
args = { MappedStatement.class, 
         Object.class,
         RowBounds.class, 
         ResultHandler.class }) })
public class TotalCountBoundsBeanInterceptor extends PaginationInterceptor {

  @Override
  protected void setTotal(ResultBounds resultBounds, MappedStatement mappedStatement, Object parameter, Invocation invocation) {
    resultBounds.setTotal(new TotalCountBoundsBean(mappedStatement.getId(), parameter));
  }
  
}
