package com.garbagecode.resultbounds.utilty;

import java.util.List;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.session.Configuration;

public class TemporarySqlSource implements SqlSource {
  private BoundSql boundSql;

  public TemporarySqlSource(Configuration configuration, 
                            String sql, 
                            List<ParameterMapping> parameterMappings, 
                            Object parameterObject) {
    boundSql = new BoundSql(configuration, sql, parameterMappings, parameterObject);
  }

  @Override
  public BoundSql getBoundSql(Object parameterObject) {
    return boundSql;
  }

}
