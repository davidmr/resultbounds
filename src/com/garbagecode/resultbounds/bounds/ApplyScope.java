package com.garbagecode.resultbounds.bounds;

@FunctionalInterface
public interface ApplyScope {
  Object apply();
}
