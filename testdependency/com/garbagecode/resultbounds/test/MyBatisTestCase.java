package com.garbagecode.resultbounds.test;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class MyBatisTestCase {

  public SqlSession openSession() {
    String resource = "com/garbagecode/resultbounds/test/mybatis-config.xml";
    InputStream inputStream;

    try {
      inputStream = Resources.getResourceAsStream(resource);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    return sessionFactory.openSession();
  }

}
