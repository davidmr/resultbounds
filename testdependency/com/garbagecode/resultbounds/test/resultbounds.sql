
CREATE DATABASE IF NOT EXISTS `resultbounds`;
USE `resultbounds`;

CREATE TABLE IF NOT EXISTS `user` (
  `name` varchar(50) NOT NULL,
  `age` int(11) NOT NULL
);

INSERT INTO `user` (`name`, `age`) VALUES
  ('刘一', 23),
  ('陈二', 21),
  ('张三', 22),
  ('李四', 25),
  ('王五', 22),
  ('赵六', 21),
  ('孙七', 22),
  ('周八', 26),
  ('吴九', 24),
  ('郑十', 20);
