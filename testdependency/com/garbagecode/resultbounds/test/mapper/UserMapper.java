package com.garbagecode.resultbounds.test.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.garbagecode.resultbounds.test.User;

public interface UserMapper {

  @Select("select * from user")
  List<User> getUserList();

}
