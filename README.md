resultbounds - MyBatis分页插件
==================================================

### 该插件的可取之处 ###
在参考别人写的插件以及一年多来几次对这插件的重构，现在这插件的代码可读性还是不错的，用相对简单的方式提供了一个优雅的 MyBatis 物理分页功能。对想自己实现一个分页插件的，自认为还是挺值得参考的。

### 该插件支持的 MyBatis 版本 ###
MyBatis 3.1: 不支持

MyBatis 3.2: 不支持

MyBatis 3.3: 支持

### 在 MyBatis 配置文件中加上这个分页插件 ###
```
<configuration>
  ...
  <plugins>
    <plugin interceptor="com.garbagecode.resultbounds.PaginationInterceptor">
      <!-- 这里配的是 MySql 方言，可以自己实现 Dialec 接口，然后把 MySqlDialect 替换成自己的 -->
      <property name="dialect" value="com.garbagecode.resultbounds.dialect.MySqlDialect" />
    </plugin>
  </plugins>
    ...
</configuration>
  ```


### 在代码中使用这个插件进行分页的示例 ###
```
// 获取 SqlSession 对象
SqlSession session = ...
// 从第 4 条开始，取 5 条记录
ResultBounds resultBounds = new ResultBounds(3, 5);
// 获取查询结果
List<User> userList = session.selectList("test.user.selectUser", null, resultBounds);
// 获取记录总数
long total = resultBounds.getTotal(session);
```
对 Mapper 接口分页
```
// 获取 SqlSession 对象
SqlSession session = ...
// 从第 4 条开始，取 5 条记录
ResultBounds resultBounds = new ResultBounds(3, 5);
// 获取查询结果
// 注意：目前的实现，resultBounds 会对 apply 方法内的所有查询都有效
List<User> userList = resultBounds.apply(()->{
  return session.getMapper(UserMapper.class).getUserList();
});
// 获取记录总数
long total = resultBounds.getTotal(session);
```


### 若使用了 mybatis-spring-xxxx.jar 包整合了 Spring ###
在 MyBatis 配置文件中加上该插件的 Spring 版本
```
...
  <plugins>
    <!-- 分页插件 -->
    <plugin interceptor="com.garbagecode.resultbounds.spring.TotalCountBoundsBeanInterceptor">
      <property name="dialect" value="com.garbagecode.resultbounds.dialect.MySqlDialect" />
    </plugin>
  </plugins>
...
```
在 Spring 配置文件中加上这个
```
...
  <!-- 分页插件的 spring 版本 -->
  <bean class="com.garbagecode.resultbounds.spring.TotalCountBoundsBean">
    <property name="sqlSession" ref="sqlSession" />
  </bean>
...
```
使用该插件的 Spring 版本后
```
...
// 获取记录总数
// 调用 ResultBounds.getTotal 方法的时候不需要给 SqlSession 参数了
long total = resultBounds.getTotal(null);
...
```


### 说明该插件各个类是干嘛用的 ###
```
---- bounds
  |---- ApplyScore                      ResultBounds.apply 有用到这个接口
  |---- ResultBounds                    继承 RowBounds 类并加了些有用的方法 
  |---- TotalCountBounds                用于统计记录总数
---- dialect
  |---- Dialect                         生成分页语句、统计记录总数语句的接口
  |---- MySqlDialect                    Dialect 接口的 MySql 实现
---- spring
  |---- TotalCountBoundsBean
  |---- TotalCountBoundsBeanInterceptor 这插件的 spring 版本
---- utility
  |---- MappedStatementModifier         提供改造 MappedStatement 对象的方法
  |---- TemporarySqlSource
---- AbstractPaginationInterceptor  
---- PaginationInterceptor              分页的主逻辑在这个类里
```